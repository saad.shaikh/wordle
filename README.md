# Title: Wordle

## Description
This is a Wordle app that allows you to play multiple times in a day.

## Tech stack
- Svelte
- TypeScript
- API
- Vite

## Deployed project
https://saad-shaikh-wordle.netlify.app/